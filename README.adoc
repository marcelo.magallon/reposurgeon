= reposurgeon - a repository surgeon =

`reposurgeon` enables risky operations that version-control systems
don't want to let you do, such as (a) editing past comments and metadata,
(b) excising commits, (c) coalescing commits, and (d) removing files and
subtrees from repo history. The original motivation for `reposurgeon`
was to clean up artifacts created by repository conversions.

`reposurgeon` is also useful for scripting very high-quality
conversions from Subversion.  It is better than `git-svn` at tag
lifting, automatically cleaning up `cvs2svn` conversion artifacts,
dealing with nonstandard repository layouts, recognizing branch
merges, handling mixed-branch commits, and generally at coping with
Subversion's many odd corner cases.  Normally Subversion repos should
be analyzed at a rate of upwards of ten thousand commits per minute,
though that rate can fall significantly on extremely large
repositories.

An auxiliary program, `repotool`, performs various useful
operations such as checkouts and tag listing in a VCS-independent
manner.  Yet another, `repomapper`, assists in automatically preparing
contributor maps of CVS and SVN repositories.

The `repocutter` program is available for some specialized operations on
Subversion dumpfiles; it may be useful in extracting portions of
particularly gnarly Subversion repositories for conversion witth
reposurgeon.

This distribution supports a generic conversion workflow using these
tools, and includes the DVCS Migration Guide that describes how to use it.

The file 'reposurgeon-git-aliases` can be appended to your `~/.gitconfig' to
support working directly with action stamps in git.

Finally, an Emacs Lisp mode with useful functions for editing large
comment message-boxes is included.

The hashbang lines in these tools invoke PyPy, an alternative Python
implementation that performs much better than CPython on this code. You
can use python2 or python3 to run these scripts, but it is best to
have pypy installed.  The performance difference is dramatic on large
repositories.

We are in the process of moving this suite of tools from Python to Go
for large performance increases.  The subdirectory 'src' contains the
Go source.  Do your builds through the Makefile, which sets the GOPATH
variable to work around it not being embedded in a normal Go
workspace.

To build the Go tools, simply run "make" in the toplevel directory.
This also builds the suite documentation.  Before running it the
*first* time, run "make gosetup" to install package dependencies.

There is an extensive regression-test suite in the `test/` directory.
To test the correctness of this software, ensure that `pypy` and
`pylint` and `golang` are installed and then type `make check`.  For a
portability check that includes Python 2 and Python 3 as well, do
"make portcheck".

See `reporting-bugs.asc` for advice on how to troubleshoot problems
with `reposurgeon` and report bugs.

The main `reposurgeon` website along with the documentation in HTML files
lives at http://www.catb.org/esr/reposurgeon/[www.catb.org/esr/reposurgeon/].

The files Dockerfile, .dockerignore, .gitlab-ci.yml, and the contents of ci/
are not distributed; they are configuration for test builds on GitLab's
CI machinery.
